import falcon
import sqlite3
import json

conn = sqlite3.connect('resolutions.db')
lookup = {}
countryCount = 0

def initLookup():
    cursor = conn.cursor()
    for row in cursor.execute('Select key, value from Lookup'):
        lookup[row[0]] = row[1]

initLookup()
countryCount = len(json.loads(lookup['countries']))

class Search(object):
    def on_get(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin','*')
        if 'query' in req.params:
            query = req.params['query']
            query = query.replace('"',' ')
            print "Searching", query
            cursor = conn.cursor()
            votes = [[] for i in range(0,countryCount)]
            matches = [[0 for j in range(0,countryCount)] for i in range(0,countryCount)]
            resolutions = []
            for res in cursor.execute('Select number, title, link, session, votes, votingRecord, adoptedOn from resolution where number in (Select number from resolution_search where title match ?)', (query, )):
                details = {'n':res[0],'t':res[1],'l':res[2],'s':res[3],'d':res[6]}
                if res[4] != None:
                    details['v'] = res[4]
                if res[5] != None:
                    record = json.loads(res[5])
                    details['r'] = record
                    for i in range(0,countryCount):
                        if str(i) in record:
                            votes[i].append(record[str(i)])
                        else:
                            # We only consider -1, 0, 1 i.e No, Abstain, Yes for analysis
                            # So add 10 to ignore the lack of a vote
                            votes[i].append(10)
                resolutions.append(details)
            if len(resolutions) > 0:
                voteCount = len(votes[0])
                if voteCount > 0:
                    for i in range(0,countryCount-1):
                        for j in range (i+1,countryCount):
                            match = 0
                            oppose = 0
                            for k in range(0,voteCount):
                                if votes[i][k] * votes[j][k] == -1:
                                    oppose = oppose + 1
                                elif votes[i][k] == votes[j][k]:
                                    if votes[i][k] in [-1,0,1] and votes[j][k] in [-1,0,1]:
                                        match = match + 1
                            if oppose > match:
                                matches[i][j] = matches[j][i] = int(-10.0*oppose/voteCount)
                            elif match > oppose:
                                matches[i][j] = matches[j][i] = int(10.0*match/voteCount)
                resp.content_type = 'application/json'
                resp.cache_control = ['public','max-age=72000']
                resp.body = json.dumps({'countries':json.loads(lookup['countries']),'resolutions':resolutions,'matches':matches})

class Dummy(object):
    def on_get(self, req, resp):
        resp.set_header('Access-Control-Allow-Origin','*')

search = Search()
dummy = Dummy()
app = falcon.API()
app.add_route('/search', search)
app.add_route('/keepalive', dummy)