# Execute using python -i scrape.py and call the appropriate function
# Typical order when starting from scratch would be 
# scrapeSessions()
# parseSessions()
# initVoteScraping()
#
# If starting from scratch it would be best to combine scrapeVotes and findMissingVotes into a single function
# scrapeVotes()
# findMissingVotes()
# scrapeVotes()
#
# parseVotes()
# createSearchTable()
#
# Finally delete scraped data
# dropScrapedData()

import sqlite3
import requests
from bs4 import BeautifulSoup
import re, time, sys, json
import urllib
import copy

conn = sqlite3.connect('resolutions.db')
cursor = conn.cursor()

def scrapeSessions():
    cursor.execute('Drop table if exists session')
    cursor.execute('Create table session (number integer, year integer, link text, pagetext text)')
    for i in range(1,72):
        link = 'http://www.un.org/depts/dhl/resguide/r' + str(i) + '_resolutions_table_en'
        if i<68:
            link = link + 'g'
        link = link + '.htm'
        print link
        r = requests.get(link)
        if r.status_code == 200:
            cursor.execute('INSERT INTO session VALUES (:id,:year,:link,:pagetext)',{'id':i, 'year':i+1945, 'link':link, 'pagetext':r.text})
        else:
            print r.status_code
            raise Exception
        conn.commit()

def parseSessions():
    print "Parsing"
    cursor.execute('Drop table if exists resolution')
    cursor.execute('Create table resolution (number text primary key, session integer, adoptedOn text, votes text, foundVotingDetails number, hasVotingRecord number, votingRecord text, title text, link text)')
    for row in cursor.execute('Select * from session'):
        sessionId = row[0]
        html = row[3]
        html = re.sub('<[/]*br?>','',html,flags=re.I)
        html = re.sub('\\r\\n',' ',html)
        if sessionId == 1:
            html = re.sub('</a></a>','</a>',html)
        if sessionId == 3 or sessionId == 16:
            html = re.sub('</td>\\s*<tr>','</td></tr><tr>',html)
            html = re.sub('</table>.*$','</table>',html)
        if sessionId == 34:
            html = re.sub('<tr>\\s*<tr>','<tr>',html)
        if sessionId == 55:
            html = re.sub('</td>\\s*<tr>\\s*<tr>','</td></tr><tr>',html)
        if sessionId == 65 or sessionId == 66 or sessionId == 68 or sessionId == 69:
            html = re.sub('<td>[^<]*</a>','<td>',html)
        if sessionId == 68:
            html = re.sub('<a target="_top"/>','</a>',html)
        soup = BeautifulSoup(html, 'html.parser')
        table = soup('tbody')[0]
        trs = table('tr')[1:]
        headers = trs[0]('th')
        voteindex = -1
        for i in range(0,len(headers)):
            header = headers[i]
            if re.search('vote',header.text,re.I):
                voteindex = i
                break
        resolutions = trs[1:]
        for resolution in resolutions:
            if len(resolution('td')) != len(headers):
                if sessionId == 70:
                    continue
                print sessionId
                print resolution
            details = resolution('td')
            resno = re.sub('^\\s*','',details[0].text)
            resno = re.sub('\\s*$','',resno)
            if resno == 'A/RES/50/125':
                continue
            if not details[0].a == None:
                link = details[0].a.get('href')
            else:
                link = ''
            # Some resolutions do not have the link and use '#' instead, do not store their link
            m = re.match('http://www.un.org/en/ga/search/view_doc.asp\?symbol=(.*)$', link)
            if m:
                link = m.groups()[0]
            else:
                print 'Unable to parse link', link, 'for resolution', resno, 'Storing as empty string'
                link = ''
            votes = re.search('\\d+\\s*-\\s*\\d+\\s*-\\s*\\d+',details[voteindex].text)
            if votes != None:
                votes = votes.group()
            datetd = copy.copy(details[voteindex])
            if datetd.a != None:
                datetd.a.decompose()
                if datetd.text == None or datetd.text == '' or datetd.text == ' ':
                    datetd = copy.copy(details[voteindex])
            date = re.search('(?:(?<=^)|(?<=[^\d]))\d{1,2}[^\d]+(?:'+str(sessionId+1945)+'|'+str(sessionId+1946)+')',datetd.text)
            if date != None:
                resDate = date.group()
                print resno, resDate
            else:
                if resno == 'A/RES/41/1':
                    resDate = '10 Oct. 1986'
                elif resno in ['A/RES/71/277','A/RES/71/276','A/RES/71/275']:
                    resDate = '2 February 2017'
                else:
                    print "Not found date for", resno,':',details[voteindex].text,':',datetd.text,' Session', sessionId
                    raise Exception
            title = re.sub('\\s{2,}',' ',details[len(headers)-1].text)
            if resno == 'A/RES/43/88' and sessionId == 34:
                resno = 'A/RES/34/88'
            if resno == 'A/RES/289(IV)A' and title == 'Question of the disposal of the former Italian colonies LYBIA':
                resno = 'A/RES/289(IV)AA'
            try:
                conn.execute('insert into resolution values(:resno,:sessionId,:date,:votes,NULL,NULL,NULL,:title,:link)',{'resno':resno, 'sessionId':sessionId, 'date':resDate, 'votes':votes, 'title': title, 'link':link})
            except sqlite3.IntegrityError:
                print "UNIQUE constraint failed... Skipping",resno, votes, title, link
    conn.execute("update resolution set number = 'A/RES/65/303' where number = 'AA/RES/65/303'")
    conn.execute("update resolution set number = 'A/RES/65/300' where number = 'AA/RES/65/300'")
    conn.execute("update resolution set number = 'A/RES/65/256 B' where number = 'AA/RES/65/256 B'")
    conn.execute("update resolution set number = 'A/RES/60/244' where number = '>A/RES/60/244'")
    conn.execute("update resolution set number = 'A/RES/2237(XXI)' where number = 'A/RES/2237(XXI))'")
    conn.execute("update resolution set number = 'A/RES/38/183 J' where number = 'A/RES/38/183 J*B108'")
    conn.commit()

def initVoteScraping():
    cursor.execute('Drop table if exists votescrape')
    cursor.execute('Create table votescrape (number text primary key, link text, pagetext text)')
    for row in cursor.execute('Select number from resolution where votes is not null'):
        resno = row[0]
        term = re.sub('[\s]','',resno)
        link = 'http://unbisnet.un.org:8080/ipac20/ipac.jsp?profile=voting&index=.VM&term=' + term
        conn.execute('insert into votescrape values (:resno, :link, NULL)',{'resno':resno, 'link':link})
    conn.commit()

def scrapeVotes():
    try:
        for row in cursor.execute('Select number, link from votescrape where pagetext is NULL'):
            resno = row[0]
            link = row[1]
            print resno, link,
            retry = True
            while retry:
                try:
                    r = requests.get(link, timeout=5)
                    retry = False
                except:
                    conn.commit()
                    print 'Timeout, retrying ...'
            print r.status_code
            if r.status_code == 200:
                conn.execute('Update votescrape set pagetext = ? where number = ?',(r.text, resno, ))
            else:
                print r.status_code
                raise Exception
            time.sleep(5)
    except:
        print 'Committing due to exception'
        conn.commit()
    else:
        conn.commit()
        print 'Done'

def parseMissingVotePageLinks(resno, soup):
    links = soup('a')
    resolutionLink = ''
    nextPageLink = ''
    for link in links:
        if link.text == resno or re.sub('[\[\]]','',link.text) == re.sub('[\[\]\s]','',resno):
            resolutionLink = re.sub('ipac.jsp[^&]*&','ipac.jsp?',urllib.unquote(link.attrs['href'].split("'")[1]))
        if link.text == 'Next' and nextPageLink == '':
            nextPageLink = re.sub('ipac.jsp[^&]*&','ipac.jsp?',urllib.unquote(link.attrs['href'].split("'")[1]))
    return (resolutionLink, nextPageLink)

def findMissingVotes():
    for row in cursor.execute('Select number, link, pagetext from votescrape where pagetext is not null'):
        foundVotes = 0
        resno = row[0]
        originalLink = row[1]
        html = row[2]
        soup = BeautifulSoup(html, 'html.parser')
        for form in soup('form'):
            if 'name' in form.attrs and form.attrs['name'] == 'full':
                foundVotes = 1
        if foundVotes == 0:
            resolutionLink, nextPageLink = parseMissingVotePageLinks(resno, soup)
            while resolutionLink =='' and nextPageLink != '':
                print "Link for", resno, "not found. Searching next page", nextPageLink
                retry = True
                while retry:
                    try:
                        r = requests.get(nextPageLink, timeout=5)
                        retry = False
                    except:
                        print 'Timeout, retrying ...'
                print r.status_code
                if r.status_code == 200:
                    resolutionLink, nextPageLink = parseMissingVotePageLinks(resno, BeautifulSoup(r.text, 'html.parser'))
                    time.sleep(5)
            if resolutionLink != '':
                print "Found link for", resno, resolutionLink
                conn.execute('Update votescrape set link = ?, pagetext = NULL where number = ?', (resolutionLink, resno, ))
                conn.commit()
            else:
                if re.sub('-','%20',originalLink) != originalLink and re.sub('A-[A-Z]','',resno) == resno:
                    print "Failed to find links for",resno,", trying modified link", re.sub('-','%20',originalLink)
                    conn.execute('Update votescrape set link = ?, pagetext = NULL where number = ?', (re.sub('-','%20',originalLink), resno, ))
                    conn.commit()
                else:
                    print "Failed to find link for", resno

def parseResolutionVote(resno, soup, countries):
    voteForm = None
    hasRecord = None
    record = None
    for form in soup('form'):
        if 'name' in form.attrs and form.attrs['name'] == 'full':
            voteForm = form
            break
    if voteForm:
        for tr in voteForm('tr'):
            tds = tr('td')
            if re.search('^Vote Notes:',tds[0].text):
                if re.search('^NON-RECORDED',tds[1].text) or resno in ['A/RES/409(V)A','A/RES/799(VIII)']:
                    hasRecord = False
                elif re.search('^(?:RECORDED|ROLL-?CALL)',tds[1].text) or resno in ['A/RES/43/62']:
                    hasRecord = True
                elif resno in ['A/RES/36/123','A/RES/39/65 D','A/RES/39/65 C','A/RES/49/44','A/RES/49/42','A/RES/49/37','A/RES/49/35 B','A/RES/49/34','A/RES/49/29','A/RES/54/61']:
                    voteForm = None
                    hasRecord = None
                    break
                else:
                    print 'Unable to parse vote notes:', tds[1].text, 'Continue?'
                    ans = raw_input()
                    if ans not in ['Y','y']:
                        raise Exception
                    voteForm = None
                    hasRecord = None
            if re.search('^Detailed Voting:',tds[0].text):
                record = tds[1]
        if resno in ['A/RES/32/165','A/RES/31/15A','A/RES/914(X)','A/RES/947(X)','A/RES/948(X)','A/RES/969(X)','A/RES/970(X)','A/RES/974(X)','A/RES/975(X)','A/RES/976(X)','A/RES/977(X)','A/RES/978(X)','A/RES/979(X)','A/RES/980(X)','A/RES/981(X)','A/RES/982(X)']:
            voteForm = None
            hasRecord = None
    if voteForm == None:
        print "Votes not found"
        conn.execute('Update resolution set foundVotingDetails = 0 where number = ?',(resno,))
        conn.commit()
        return
    else:
        if hasRecord == None:
            print 'Unable to parse vote. Was voting recorded?'
            raise Exception
        elif hasRecord == False:
            print 'Non Recorded'
            conn.execute('Update resolution set foundVotingDetails = 1, hasVotingRecord = 0 where number = ?',(resno,))
            conn.commit()
            return
        else:
            if record == None:
                print 'Voting record expected but not found, Continue?'
                ans = raw_input()
                if ans not in ['Y','y']:
                    raise Exception
            else:
                print 'Recorded'
                votes = record('a')
                voteRecord = {}
                for a in votes:
                    atext = a.text
                    if atext == 'Aa UNITED STATES' and resno == 'A/RES/2554(XXIV)':
                        atext = 'A UNITED STATES'
                    if atext == 'AN PAKISTAN' and resno == 'A/RES/620(VII) A':
                        atext = 'A PAKISTAN'
                    if atext == 'AY UNION OF SOUTH AFRICA' and resno == 'A/RES/1605(XV)':
                        atext = 'A UNION OF SOUTH AFRICA'
                    if atext == 'NY PAKISTAN' and resno == 'A/RES/32/15':
                        atext = 'N PAKISTAN'
                    if atext == 'AY SWEDEN' and resno == 'A/RES/34/113':
                        atext = 'A SWEDEN'
                    if atext == 'AY DENMARK' and resno == 'A/RES/36/146B':
                        atext = 'A DENMARK'
                    match = re.match('^([^\s])\s+(.*)$', atext)
                    if match:
                        country = match.groups()[1].lstrip().rstrip().upper()
                        vote = match.groups()[0]
                        if vote in ['Y','y']:
                            vote = 1
                        elif vote in ['A','a']:
                            vote = 0
                        elif vote in ['N','n']:
                            vote = -1
                        elif vote == '9':
                            vote = 9
                        else:
                            print atext
                            raise Exception
                    else:
                        country = atext.lstrip().rstrip().upper()
                        # Use -9 to denote that a country has not voted
                        vote = -9
                    if country in ["BOLIVIA (PLURINATIONAL STATE OF)","PLURINATIONAL STATE OF BOLIVIA", "BOLIVIA (PLUNATIONAL STATE OF)", "BOLIVIA (PLURINATIOANL STATE OF)"]:
                        country = 'BOLIVIA'
                    elif country == 'SURINAM':
                        country = 'SURINAME'
                    elif country == 'IRAN (ISLAMIC REPUBLIC OF)':
                        country = 'IRAN'
                    elif country == "COTE D'IVOIRE":
                        country = 'IVORY COAST'
                    elif country == 'VENEZUELA (BOLIVARIAN REPUBLIC OF)':
                        country = 'VENEZUELA'
                    elif country == 'THE REPUBLIC OF KAZAKHSTAN':
                        country = 'KAZAKHSTAN'
                    elif country in ['REPUBLIC OF MOLDOVA','REPULIC OF MOLDOVA']:
                        country = 'MOLDOVA'
                    elif country == 'KYRGYZTAN':
                        country = 'KYRGYZSTAN'
                    elif country == "LAO PEOPLE'S DEMOCRATIC REPUBLIC":
                        country = 'LAOS'
                    elif country == 'PHILIPPINE REPUBLIC':
                        country = 'PHILIPPINES'
                    elif country == 'UNION OF SOUTH AFRICA':
                        country = 'SOUTH AFRICA'
                    elif country == 'MALDIVE ISLANDS':
                        country = 'MALDIVES'
                    elif country in ['LIBYAN ARAB REPUBLIC','LIBYAN ARAB JAMAHIRIYA']:
                        country = 'LIBYA'
                    elif country == 'SYRIAN ARAB REPUBLIC':
                        country = 'SYRIA'
                    elif country == 'UNITED REPUBLIC OF CAMEROON':
                        country = 'CAMEROON'
                    elif country == 'REPUBLIC OF NAURU':
                        country = 'NAURU'
                    elif country == 'KINGDOM OF TONGA':
                        country = 'TONGA'
                    elif country == 'REPUBLIC OF KIRIBATI':
                        country = 'KIRIBATI'
                    elif country == 'CABO VERDE':
                        country = 'CAPE VERDE'
                    elif country == 'SERBIAMONTENEGRO':
                        country = 'SERBIA AND MONTENEGRO'
                    if country not in countries:
                        countries.append(country)
                    voteRecord[countries.index(country)] = vote
                conn.execute('Update resolution set foundVotingDetails = 1, hasVotingRecord = 1, votingRecord = ? where number = ?',(json.dumps(voteRecord),resno,))
                conn.commit()
                return

def parseVotes():
    cursor.execute('Create table if not exists Lookup (key text, value text)')
    cursor.execute('Delete from lookup where key="countries"')
    countries = []
    print 'Parsing votes'
    for row in cursor.execute('Select number, pagetext from votescrape'):
        resno = row[0]
        print resno,
        html = row[1]
        soup = BeautifulSoup(html, 'html.parser')
        parseResolutionVote(resno, soup, countries)
    cursor.execute('insert into lookup values ("countries", ?)',(json.dumps(countries),))
    conn.commit()

def createSearchTable():
    cursor.execute('Drop table if exists resolution_search')
    cursor.execute('Create virtual table resolution_search using fts4(number text, title text, tokenize=porter)')
    cursor.execute('Insert into resolution_search(number,title) select number,title from resolution')
    conn.commit()

def dropScrapedData():
    cursor.execute('Drop table if exists session')
    cursor.execute('Drop table if exists votescrape')